all: cv-main

cv-main: main.tex avatar.png
	latexmk -pdf -interaction=nonstopmode -jobname=cv main.tex

.PHONY: clean

clean:
	rm cv.*
